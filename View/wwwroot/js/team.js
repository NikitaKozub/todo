﻿"use strict";

const hubTeamConnection = new signalR.HubConnectionBuilder()
    .withUrl("/teamHub", { transport: signalR.HttpTransportType.WebSockets | signalR.HttpTransportType.LongPolling })
    .configureLogging(signalR.LogLevel.Information)
    .build();

// получение сообщения от сервера
// Удаление члена команды из списка членов команды
// Если вы удалены появиться сообщение о вашем исключение из команды
hubTeamConnection.on("deleteMember", function (test) {
    if (test.nameUser == nameUser.innerText) {
        deleteElementTeam();
        messageMember("Вас исключили из команды");
    }
    let select = document.getElementById("SelectMemeber");
    select.options[test.idSelected] = null;
});

hubTeamConnection.on("joinMember", function (newMember) {
    let select = document.getElementById("SelectMemeber");
    var text = newMember.nameUser;
    var value = newMember.idSelected;
    var newOption = new Option(text, value);
    select.add(newOption);
});

hubTeamConnection.on("deleteLeaderMember", function (test) {
    //группа закрыта
    deleteElementTeam();
    messageMember("Вас исключили из команды");
});

hubTeamConnection.on("joinTeam", function (team) {
    location.reload();
});

// отправка сообщения на сервер
if (document.getElementById("DeleteMemeberTeam") != null) {
    document.getElementById("DeleteMemeberTeam").addEventListener("click", function (e) {
        var selectedItr = document.getElementById("SelectMemeber").options.selectedIndex;
        var selectedName = document.getElementById("SelectMemeber").options[selectedItr].innerText;
        
        hubTeamConnection.invoke("SendDeleteMemeberTeam", { "IdSelected": selectedItr, "NameUser": selectedName });
    });
}

// Сообщение членам команды
function messageMember(messageText) {
    var main = document.querySelector("main");
    var divElem = document.createElement("div");
    divElem.className = "messageImportant";
    divElem.innerText = messageText;
    main.prepend(divElem);
}

// удаление элементов связанных с членством команды(состав комнады и чат)
function deleteElementTeam() {
    let teamDelete = document.getElementById("team");
    let chatDelete = document.getElementById("chat");
    while (teamDelete.firstChild) {
        teamDelete.removeChild(teamDelete.firstChild);
    }
    while (chatDelete.firstChild) {
        chatDelete.removeChild(chatDelete.firstChild);
    }
}

function findJoinButtons() {
    var joinButtons = document.querySelectorAll(".JoinTeam");
    for (let i = 0; i < joinButtons.length; i++) {
        joinButtons[i].addEventListener("click", function () {
            hubTeamConnection.invoke("JT", this.id);
        });
    }
}

if (document.getElementById("theCompany") != null) {
    findJoinButtons();
}


hubTeamConnection.start();