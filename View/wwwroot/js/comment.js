﻿"use strict";

const hubConnection = new signalR.HubConnectionBuilder()
    .withUrl("/chatHub", { transport: signalR.HttpTransportType.WebSockets | signalR.HttpTransportType.LongPolling })
    .configureLogging(signalR.LogLevel.Information)
    .build();

var chatroom = document.getElementById("comments");

// Прокрутка вниз
function scrollDown() {
    chatroom.scrollTop = chatroom.scrollHeight;
}
scrollDown();

// получение сообщения от сервера
hubConnection.on("commentMessage", function (comment) {
    let elem = document.getElementById('comments');
    var divElem = document.createElement("div");
    divElem.className = "chatMessage";
    divElem.innerHTML = "<div class='userName'>" + comment.loginUser + "</div>"
        + "<div class='textMessage'>" + comment.text + "</div>";
    elem.appendChild(divElem);
    scrollDown();
});

// отправка сообщения на сервер
document.getElementById("WriteComment").addEventListener("click", function (e) {
    let messageInput = document.getElementById("messageInput").value;
    let idCase = document.getElementById("HiddenFieldIdCase").value;
    hubConnection.invoke("SendCommentMessage", { "LoginUser": "", "Text": messageInput, "IdCase": idCase });
    document.getElementById("messageInput").value = "";
});

hubConnection.start();