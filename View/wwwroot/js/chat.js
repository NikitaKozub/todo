﻿"use strict";

var comments = document.getElementById("comments");

var chatOpen = document.getElementById("chatOpening");
chatOpen.addEventListener("click", function() { openClose(); }, false);

// Скрыть/показать чат
function openClose() {
    var chat = document.getElementById('chat');
    chat.hidden = !chat.hidden;
    if (chat.hidden == false) {
        scrollDown();
    }
}

// Прокрутка вниз
function scrollDown() {
    comments.scrollTop = comments.scrollHeight;
}

const hubConnection = new signalR.HubConnectionBuilder()
    .withUrl("/chatHub", { transport: signalR.HttpTransportType.WebSockets | signalR.HttpTransportType.LongPolling })
    .configureLogging(signalR.LogLevel.Information)
    .build();

// получение сообщения от сервера
hubConnection.on("chatMessage", function (user) {
    let elem = document.querySelector('#comments');
    var divElem = document.createElement("div");
    divElem.className = "chatMessage";
    divElem.innerHTML = "<div class='userName'>" + user.loginUser + "</div>"
        + "<div class='textMessage'>" + user.text + "</div>";

    elem.appendChild(divElem);
    scrollDown();
});

function sendMessageChat() {
    let messageInput = document.getElementById("messageInput").value;
    if (String(messageInput).length > 0) {
        hubConnection.invoke("SendChatMessage", { "Text": messageInput });
        document.getElementById("messageInput").value = "";
    }
}

// отправка сообщения на сервер
document.getElementById("sendButton").addEventListener("click", function (e) {
    sendMessageChat();
});

$("#messageInput").keyup(function (event) {
    if (event.keyCode == 13) {
        sendMessageChat();
    }
}); 

hubConnection.start();