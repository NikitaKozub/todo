﻿"use strict";

const hubStatusCaseConnection = new signalR.HubConnectionBuilder()
    .withUrl("/statusHub", { transport: signalR.HttpTransportType.WebSockets | signalR.HttpTransportType.LongPolling })
    .configureLogging(signalR.LogLevel.Information)
    .build();

//ставит цвет задания в зависимости от статуса
//alt - описания статуса задания
//id - номер задания
function statusColor(alt, id) {
    if (alt == "confirmed") {
        document.getElementById("cardStatus " + id).style.backgroundColor = 'Green';
        return;
    }
    if (alt == "no_complete") {
        document.getElementById("cardStatus " + id).style.backgroundColor = 'Red';
        return;
    }
    if (alt == "complete") {
        document.getElementById("cardStatus " + id).style.backgroundColor = 'Yellow';
        return;
    }
}

// инициализирует кнопки провала задания
function findFailedButtons() {
    let failedButtons = document.querySelectorAll(".failed");
    for (let i = 0; i < failedButtons.length; i++) {
        failedButtons[i].addEventListener("click", function () {
            hubStatusCaseConnection.invoke("ChangeStatusCase", { "IdCase": this.id, "ClassName": "imgCard", "SrcImage": "/images/no_complete.png", "AltImage": "no_complete" });
        });
    }
}

// инициализирует кнопки подтверждения задания
function findConfirmationButtons() {
    let confirmationButtons = document.querySelectorAll(".confirmed");
    for (let i = 0; i < confirmationButtons.length; i++) {
        confirmationButtons[i].addEventListener("click", function () {
            hubStatusCaseConnection.invoke("ChangeStatusCase", { "IdCase": this.id, "ClassName": "imgCard", "SrcImage": "/images/confirmed.png", "AltImage": "confirmed" });   
        });
    }
}

// инициализирует кнопки отмены задания
function findCancelButtons() {
    let cancelButtons = document.querySelectorAll(".cancel");
    for (let i = 0; i < cancelButtons.length; i++) {
        cancelButtons[i].addEventListener("click", function () {
            hubStatusCaseConnection.invoke("ChangeStatusCase", { "IdCase": this.id, "ClassName": "imgCard", "SrcImage": "cancel", "AltImage": "failed" });
        });
    }
}

// инициализирует кнопки выполненого задания
function findCompleteButtons() {
    var completeButtons = document.querySelectorAll(".complete");
    for (let i = 0; i < completeButtons.length; i++) {
        completeButtons[i].addEventListener("click", function () {
            hubStatusCaseConnection.invoke("ChangeStatusCase", { "IdCase": this.id, "ClassName": "imgCard", "SrcImage": "/images/complete.jpg", "AltImage": "complete" });            
        });
    }
}

//Определяет статус задания
function setStatus() {
    let status = document.querySelectorAll(".cardStatus");
    if (status.length != 0) {
        for (let i = 0; i < status.length; i++) { 
            let idCase = +/\d+/.exec(status[i].id);
            let src = document.getElementById("imgCard " + idCase);
            statusColor(src.alt, idCase);
        }
    }
}

hubStatusCaseConnection.on("changeStatus", function (codStatus) {
    if (codStatus.srcImage == "cancel") {
        document.getElementById("case " + codStatus.idCase).remove();// элемент li в котором лежит изображение
    }

    let ImgId = document.getElementById("caseImg " + codStatus.idCase);// элемент li в котором лежит изображение
    let newImgStatus = document.createElement("img");// новое изображение на замену
    newImgStatus.className = codStatus.className;
    newImgStatus.src = codStatus.srcImage;
    newImgStatus.alt = codStatus.altImage;
    
    while (ImgId.firstChild) {
        ImgId.removeChild(ImgId.firstChild);
    }
    ImgId.appendChild(newImgStatus);

    statusColor(codStatus.altImage, codStatus.idCase);
});

hubStatusCaseConnection.on("changeButton", function (codStatus) {
    if (codStatus.srcImage == "/images/confirmed.png") {
        let elementStatusCase = document.getElementById("buttonStatusCase");
        while (elementStatusCase.firstChild) {
            elementStatusCase.removeChild(elementStatusCase.firstChild);
        }
    }

    if (codStatus.srcImage == "/images/no_complete.png") {
        let isConfirmed = document.getElementsByClassName("confirmed button " + codStatus.idCase)[0];
        if (isConfirmed != null) {
            isConfirmed.remove();
        }
    }

    if (codStatus.srcImage == "/images/complete.jpg") {
        let elementStatusCase = document.getElementById("divConfirmed");
        let buttonStatus = document.createElement("div");
        buttonStatus.className = "confirmed button " + codStatus.idCase;
        buttonStatus.id = codStatus.idCase;
        buttonStatus.textContent = "Подтвердить";
        elementStatusCase.appendChild(buttonStatus);
        findConfirmationButtons()
    }
});

findConfirmationButtons();// инициализация кнопки подтверждения
findCompleteButtons();// инициализация кнопки выполнения задания
findFailedButtons();// инициализация кнопки провала задания
findCancelButtons();// инициализация кнопки отмены задания
setStatus();// инициализация цветового обозначения статуса задания

hubStatusCaseConnection.start();
