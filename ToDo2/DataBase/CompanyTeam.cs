﻿using System;
using System.Collections.Generic;

namespace ToDo2.DataBase
{
    public partial class CompanyTeam
    {
        public int Id { get; set; }
        public int IdCompany { get; set; }
        public int IdTeam { get; set; }

        public virtual Company IdCompanyNavigation { get; set; }
        public virtual Team IdTeamNavigation { get; set; }
    }
}
