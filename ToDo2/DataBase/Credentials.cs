﻿using System;
using System.Collections.Generic;

namespace ToDo2.DataBase
{
    public partial class Credentials
    {
        public Credentials()
        {
            ClientToDo = new HashSet<ClientToDo>();
        }

        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public virtual ICollection<ClientToDo> ClientToDo { get; set; }
    }
}
