﻿using System;
using System.Collections.Generic;

namespace ToDo2.DataBase
{
    public partial class TeamCase
    {
        public int Id { get; set; }
        public int IdTeam { get; set; }
        public int IdCase { get; set; }

        public virtual Case IdCaseNavigation { get; set; }
        public virtual Team IdTeamNavigation { get; set; }
    }
}
