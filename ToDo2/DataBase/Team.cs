﻿using System;
using System.Collections.Generic;

namespace ToDo2.DataBase
{
    public partial class Team
    {
        public Team()
        {
            Chat = new HashSet<Chat>();
            ClientToDo = new HashSet<ClientToDo>();
            CompanyTeam = new HashSet<CompanyTeam>();
            TeamCase = new HashSet<TeamCase>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? Leader { get; set; }
        public int? IdCompany { get; set; }

        public virtual Company IdCompanyNavigation { get; set; }
        public virtual ICollection<Chat> Chat { get; set; }
        public virtual ICollection<ClientToDo> ClientToDo { get; set; }
        public virtual ICollection<CompanyTeam> CompanyTeam { get; set; }
        public virtual ICollection<TeamCase> TeamCase { get; set; }
    }
}
