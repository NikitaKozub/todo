﻿using System;
using System.Collections.Generic;

namespace ToDo2.DataBase
{
    public partial class Case
    {
        public Case()
        {
            Comment = new HashSet<Comment>();
            TeamCase = new HashSet<TeamCase>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Description { get; set; }
        public string IsComplete { get; set; }
        public bool Private { get; set; }
        public int IdUser { get; set; }
        public int? IdUserNavigationId { get; set; }
        public int? IdLeaderUser { get; set; }

        public virtual ClientToDo IdUserNavigation { get; set; }
        public virtual ICollection<Comment> Comment { get; set; }
        public virtual ICollection<TeamCase> TeamCase { get; set; }
    }
}
