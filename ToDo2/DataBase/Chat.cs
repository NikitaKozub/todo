﻿using System;
using System.Collections.Generic;

namespace ToDo2.DataBase
{
    public partial class Chat
    {
        public int Id { get; set; }
        public int? IdTeam { get; set; }
        public int IdUser { get; set; }
        public string Text { get; set; }

        public virtual Team IdTeamNavigation { get; set; }
        public virtual ClientToDo IdUserNavigation { get; set; }
    }
}
