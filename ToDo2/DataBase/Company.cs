﻿using System;
using System.Collections.Generic;

namespace ToDo2.DataBase
{
    public partial class Company
    {
        public Company()
        {
            ClientToDo = new HashSet<ClientToDo>();
            CompanyTeam = new HashSet<CompanyTeam>();
            Team = new HashSet<Team>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ClientToDo> ClientToDo { get; set; }
        public virtual ICollection<CompanyTeam> CompanyTeam { get; set; }
        public virtual ICollection<Team> Team { get; set; }
    }
}
