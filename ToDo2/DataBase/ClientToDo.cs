﻿using System;
using System.Collections.Generic;

namespace ToDo2.DataBase
{
    public partial class ClientToDo
    {
        public ClientToDo()
        {
            Case = new HashSet<Case>();
            Chat = new HashSet<Chat>();
            Comment = new HashSet<Comment>();
            UserRole = new HashSet<UserRole>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int IdCrendentials { get; set; }
        public int? IdCrendentialsNavigationId { get; set; }
        public int? IdCompany { get; set; }
        public int? IdTeam { get; set; }

        public virtual Company IdCompanyNavigation { get; set; }
        public virtual Credentials IdCrendentialsNavigation { get; set; }
        public virtual Team IdTeamNavigation { get; set; }
        public virtual ICollection<Case> Case { get; set; }
        public virtual ICollection<Chat> Chat { get; set; }
        public virtual ICollection<Comment> Comment { get; set; }
        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}
