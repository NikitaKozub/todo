﻿using System;
using System.Collections.Generic;

namespace ToDo2.DataBase
{
    public partial class Comment
    {
        public int Id { get; set; }
        public int IdCase { get; set; }
        public string Text { get; set; }
        public int? IdUser { get; set; }

        public virtual Case IdCaseNavigation { get; set; }
        public virtual ClientToDo IdUserNavigation { get; set; }
    }
}
