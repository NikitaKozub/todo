﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ToDo2.DataBase
{
    public partial class ToDoContext : DbContext
    {
        public ToDoContext()
        {
            Database.EnsureCreated();
        }

        public ToDoContext(DbContextOptions<ToDoContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        public virtual DbSet<Case> Case { get; set; }
        public virtual DbSet<Chat> Chat { get; set; }
        public virtual DbSet<ClientToDo> ClientToDo { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<CompanyTeam> CompanyTeam { get; set; }
        public virtual DbSet<Credentials> Credentials { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Team> Team { get; set; }
        public virtual DbSet<TeamCase> TeamCase { get; set; }
        public virtual DbSet<UserRole> UserRole { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=LAPTOP-KAQTAOU3\\SQLEXPRESS;Database=ToDo;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Case>(entity =>
            {
                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(32);
                entity.Property(e => e.IsComplete)
                    .IsRequired()
                    .HasMaxLength(12);
                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasIndex(e => e.IdUserNavigationId);

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Case)
                    .HasForeignKey(d => d.IdUserNavigationId);
                modelBuilder.Entity<Case>()
                    .HasIndex(p => new { p.Title, p.Start, p.End, p.Description, p.IdUser }).IsUnique();
            });

            modelBuilder.Entity<Chat>(entity =>
            {
                entity.Property(e => e.Text).HasMaxLength(50);

                entity.HasOne(d => d.IdTeamNavigation)
                    .WithMany(p => p.Chat)
                    .HasForeignKey(d => d.IdTeam)
                    .HasConstraintName("FK_Chat_Team");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Chat)
                    .HasForeignKey(d => d.IdUser)
                    .HasConstraintName("FK_Chat_User");
            });

            modelBuilder.Entity<ClientToDo>(entity =>
            {
                entity.HasIndex(e => e.IdCompany);

                entity.HasIndex(e => e.IdCrendentialsNavigationId);

                entity.HasIndex(e => e.IdTeam);

                entity.HasOne(d => d.IdCompanyNavigation)
                    .WithMany(p => p.ClientToDo)
                    .HasForeignKey(d => d.IdCompany)
                    .HasConstraintName("FK_ClientToDo_Company");

                entity.HasOne(d => d.IdCrendentialsNavigation)
                    .WithMany(p => p.ClientToDo)
                    .HasForeignKey(d => d.IdCrendentialsNavigationId);

                entity.HasOne(d => d.IdTeamNavigation)
                    .WithMany(p => p.ClientToDo)
                    .HasForeignKey(d => d.IdTeam)
                    .HasConstraintName("FK_ClientToDo_Team");
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.IdCaseNavigation)
                    .WithMany(p => p.Comment)
                    .HasForeignKey(d => d.IdCase)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Comment_Case");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.Comment)
                    .HasForeignKey(d => d.IdUser)
                    .HasConstraintName("FK_Comment_ClientToDO");
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<CompanyTeam>(entity =>
            {
                entity.HasIndex(e => e.IdCompany);

                entity.HasIndex(e => e.IdTeam);

                entity.HasOne(d => d.IdCompanyNavigation)
                    .WithMany(p => p.CompanyTeam)
                    .HasForeignKey(d => d.IdCompany)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CompanyTeam_Company");

                entity.HasOne(d => d.IdTeamNavigation)
                    .WithMany(p => p.CompanyTeam)
                    .HasForeignKey(d => d.IdTeam)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CompanyTeam_Team");
            });

            modelBuilder.Entity<Team>(entity =>
            {
                entity.HasIndex(e => e.IdCompany);

                entity.Property(e => e.Name)
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.HasOne(d => d.IdCompanyNavigation)
                    .WithMany(p => p.Team)
                    .HasForeignKey(d => d.IdCompany)
                    .HasConstraintName("FK_Team_Company");
            });

            modelBuilder.Entity<TeamCase>(entity =>
            {
                entity.HasIndex(e => e.IdCase);

                entity.HasIndex(e => e.IdTeam);

                entity.HasOne(d => d.IdCaseNavigation)
                    .WithMany(p => p.TeamCase)
                    .HasForeignKey(d => d.IdCase)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Case_Team");

                entity.HasOne(d => d.IdTeamNavigation)
                    .WithMany(p => p.TeamCase)
                    .HasForeignKey(d => d.IdTeam)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Team_Case");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.HasIndex(e => e.UserId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRole)
                    .HasForeignKey(d => d.UserId);
            });

            ////////////////////////////////////////////
            string adminRoleName = "admin";
            string adminEmail = "admin@mail.ru";
            string adminPassword = "123456";

            string userRoleName = "user";
            // добавляем роли
            Role adminRole = new Role { Id = 1, Name = adminRoleName };
            Role userRole = new Role { Id = 2, Name = userRoleName };

            Credentials credentialsAdmin = new Credentials { Id = 1, Login = adminEmail, Password = adminPassword };
            modelBuilder.Entity<Credentials>().HasData(credentialsAdmin);

            ClientToDo adminUser = new ClientToDo { Id = 1, Name = adminEmail, Email = adminEmail, IdCrendentials = 1 };
            UserRole userRoleAdmin = new UserRole { Id = 1, UserId = 1, RoleId = 1 };
            modelBuilder.Entity<UserRole>().HasData(userRoleAdmin);

            modelBuilder.Entity<Role>().HasData(new Role[] { adminRole, userRole });
            modelBuilder.Entity<ClientToDo>().HasData(new ClientToDo[] { adminUser });

            Company company1 = new Company();
            company1.Name = "TestCompany";
            company1.Id = 1;
            Company company2 = new Company();
            company2.Name = "TestCompany2";
            company2.Id = 2;
            modelBuilder.Entity<Company>().HasData(company1);
            modelBuilder.Entity<Company>().HasData(company2);

            base.OnModelCreating(modelBuilder);
            ////////////////////////////////////////////////////////////

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
