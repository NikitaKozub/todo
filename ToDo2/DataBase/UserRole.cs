﻿using System;
using System.Collections.Generic;

namespace ToDo2.DataBase
{
    public partial class UserRole
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }

        public virtual Role Role { get; set; }
        public virtual ClientToDo User { get; set; }
    }
}
