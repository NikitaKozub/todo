﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDo2.Models
{
    public class Message_
    {
        public string LoginUser { get; set; }
        public string Text { get; set; }
    }

    public class MessageComment : Message_
    {
        public string IdCase { get; set; }
    }
}
