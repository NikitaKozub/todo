﻿using Microsoft.AspNetCore.Http.Features;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;

namespace ToDo2.Models.Service
{
    public class TeamService
    {
        ToDoContext Context;
        UserService UserService_;
        
        public TeamService(ToDoContext context)
        {
            Context = context;
            UserService_ = new UserService(Context);
        }

        public void GetManagerTeamView(string login, out List<Case> TeamCases,
            out Team team, out List<Chat> ListChatMessage)
        {
            CaseService caseService = new CaseService(Context);
            ChatService chatService = new ChatService(Context);
            TeamCases = caseService.GetTeamCases(login);
            foreach (var itr in TeamCases)
            {
                itr.IdUserNavigation = Context.ClientToDo.Find(itr.IdUser);
            }
            ////////////////////////////////////////
            team = GetTeam(login);
            ListChatMessage = new List<Chat>();
            if (team != null)
            {
                ListChatMessage = chatService.GetChatMessages(team.Id);
                foreach (var itr in ListChatMessage)
                {
                    itr.IdUserNavigation = UserService_.GetUser(itr.IdUser);
                }
            }
        }

        /// <summary>
        /// Отображение данных в окне команд
        /// </summary>
        public void GetTeamView(string login, ChatService chatService,
            out Team YouTeam, out Company YouCompany, out bool IsLeader,
            out List<ClientToDo> UsersWithoutCompanyInTeam, out List<Team> TeamWithoutCompany,
            out List<ClientToDo> UserInCompanyInTeam, out List<ClientToDo> UsersInCompanyWithoutTeam,
            out List<Team> CompanyTeams, out List<Chat> ListChatMessage)
        {
            int idUser = UserService_.GetIdUser(login);
            CompanyTeams = GetTeams(login);
            YouTeam = GetTeam(login);
            YouCompany = UserService_.GetCompany(login);
            if (YouTeam != null && idUser == YouTeam.Leader)
            {
                IsLeader = true;
            }
            else
            {
                IsLeader = false;
            }
            UsersWithoutCompanyInTeam = new List<ClientToDo>();
            if (YouCompany == null && YouTeam != null)
            {
                UsersWithoutCompanyInTeam = GetUsersWithoutCompanyInTeam(YouTeam.Id);
            }
            TeamWithoutCompany = new List<Team>();
            if (YouCompany == null && YouTeam == null)
            {
                TeamWithoutCompany = GetTeams(login);
            }
            UserInCompanyInTeam = new List<ClientToDo>();
            if (YouCompany != null && YouTeam != null)
            {
                UserInCompanyInTeam = GetUsersInCompanyInTeam(YouCompany.Id, YouTeam.Id);
                CompanyTeams = GetTeams(login);
            }
            UsersInCompanyWithoutTeam = new List<ClientToDo>();
            if (YouCompany != null && YouTeam == null)
            {
                UsersInCompanyWithoutTeam = GetUsersInCompanyWithoutTeam(YouCompany.Id);
            }
            ////////////////////////////////////////
            //var user = UserService_.GetUser(login);
            var team = GetTeam(login);
            ListChatMessage = new List<Chat>();
            if (team != null)
            {
                ListChatMessage = chatService.GetChatMessages(team.Id);
                foreach (var itr in ListChatMessage)
                {
                    itr.IdUserNavigation = UserService_.GetUser(itr.IdUser);
                }
            }
        }

        /// <summary>
        /// Получить все команды компании
        /// </summary>
        /// <param name="nameUser">Имя пользователя</param>
        /// <returns>List<Team></Team></returns>
        public List<Team> GetTeams(string login)
        {
            ClientToDo user = UserService_.GetUser(login);
            Company company = Context.Company.FirstOrDefault(u => u.Id == user.IdCompany);
            List<Team> teams = new List<Team>();
            if (company == null)
            {
                teams = Context.Team.Where(t => t.IdCompany == null).ToList();
                return teams;
            }
            teams = Context.Team.Where(t => t.IdCompany == company.Id).ToList();
            return teams;
        }

        /// <summary>
        /// Возвращает команду пользователя
        /// </summary>
        /// <param name="name">Имя пользователя</param>
        /// <returns>object Team</returns>
        public Team GetTeam(string login)
        {
            ClientToDo user = UserService_.GetUser(login);
            if (user.IdTeam == null)
            {
                return null;
            }
            Team team = Context.Team.FirstOrDefault(c => c.Id == user.IdTeam);
            return team;
        }

        /// <summary>
        /// Получить команду пользователя
        /// </summary>
        /// <param name="idClient">id пользователя</param>
        /// <returns></returns>
        public Team GetTeam(int idClient)
        {
            ClientToDo user = UserService_.GetUser(idClient);
            if (user.IdTeam == null)
            {
                return null;
            }
            Team team = Context.Team.FirstOrDefault(c => c.Id == user.IdTeam);
            return team;
        }

        /// <summary>
        /// Проверить есть ли такая команда в компании
        /// </summary>
        /// <param name="name">Название команды</param>
        /// <returns>true - есть, false - нет</returns>
        private bool IsTeam(string nameCompany, string login)
        {
            ClientToDo user = UserService_.GetUser(login);
            Team team = Context.Team.FirstOrDefault(c => c.IdCompany == user.IdCompany && c.Name == nameCompany);
            if (team == null)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Возвращает всех участников команды
        /// </summary>
        /// <returns></returns>
        public List<ClientToDo> GetUserTeam(string login)
        {
            ClientToDo user = UserService_.GetUser(login);
            List<ClientToDo> usersOfTeam = Context.ClientToDo.Where(u => u.IdTeam == user.IdTeam).ToList();
            
            if (usersOfTeam == null)
            {
                usersOfTeam = new List<ClientToDo>();
                return usersOfTeam;
            }
            return usersOfTeam;
        }

        /// <summary>
        /// Получить пользователей без компании, без команды
        /// </summary>
        /// <returns></returns>
        public List<ClientToDo> GetUsersWithoutCompanyWithoutTeam()
        {
            List<ClientToDo> users = Context.ClientToDo.Where(c => c.IdCompany == null && c.IdTeam == null).ToList();
            if (users == null)
            {
                users = new List<ClientToDo>();
            }
            return users;
        }

        /// <summary>
        /// Получить пользователей в компании, без команды
        /// </summary>
        /// <returns></returns>
        public List<ClientToDo> GetUsersInCompanyWithoutTeam(int idCompany)
        {
            List<ClientToDo> users = Context.ClientToDo.Where(c => c.IdCompany == idCompany && c.IdTeam == null).ToList();
            if (users == null)
            {
                users = new List<ClientToDo>();
            }
            return users;
        }

        /// <summary>
        /// Получить пользователей без компании, в команде
        /// </summary>
        /// <returns></returns>
        public List<ClientToDo> GetUsersWithoutCompanyInTeam(int idTeam)
        {
            List<ClientToDo> users = Context.ClientToDo.Where(c => c.IdCompany == null && c.IdTeam == idTeam).ToList();
            if (users == null)
            {
                users = new List<ClientToDo>();
            }
            return users;
        }

        /// <summary>
        /// Получить пользователей в компании, в команде
        /// </summary>
        /// <returns></returns>
        public List<ClientToDo> GetUsersInCompanyInTeam(int idCompany, int idTeam)
        {
            List<ClientToDo> users = Context.ClientToDo.Where(c => c.IdCompany == idCompany && c.IdTeam == idTeam).ToList();
            if (users == null)
            {
                users = new List<ClientToDo>();
            }
            return users;
        }

        /// <summary>
        /// Удалить команду
        /// </summary>
        /// <param name="id">Номер команды</param>
        /// <returns></returns>
        public void DeleteTeam(int id)
        {
            var team = Context.Team.Find(id);
            if (team != null)
            {
                var users = Context.ClientToDo.Where(c => c.IdTeam == team.Id).ToList();
                foreach (var itr in users)
                {
                    itr.IdTeam = null;
                }
                Context.Chat.RemoveRange(Context.Chat.Where(c => c.IdTeam == team.Id));
                Context.TeamCase.RemoveRange(Context.TeamCase.Where(c => c.IdTeam == team.Id));
                Context.Team.Remove(team);
                Context.SaveChanges();
            }
        }

        /// <summary>
        /// Удалить пользователя из команды
        /// </summary>
        /// <param name="id">id пользователя</param>
        /// <returns>true - если лидер, false - если рядовой член</returns>
        public bool DeleteTeamMember(int id)
        {
            var user = UserService_.GetUser(id);
            var team = Context.Team.Find(user.IdTeam);
            if (team != null && team.Leader != id)
            {
                user.IdTeam = null;
                Context.SaveChanges();
                return false;
            }
            if (team != null && team.Leader == id)
            {
                DeleteTeam(team.Id);
                return true;
            }
            return false;
        }
        
        /// <summary>
        /// Присоединиться к команде
        /// </summary>
        /// <param name="id">Номер команды</param>
        /// <param name="login">Новый член команды</param>
        public void JoinTeam(int id, string login)
        {
            var user_ = UserService_.GetUser(login); ;
            user_.IdTeam = id;
            Context.SaveChanges();
        }

        /// <summary>
        /// Присоединить к команде
        /// </summary>
        /// <returns></returns>
        public void AddUserTeam()
        {
           
        }

        /// <summary>
        /// Добавить команду
        /// </summary>
        /// <param name="model"></param>
        /// <param name="login">логин</param>
        public bool AddTeam(string Name, string login)
        {
            Name = Name.Trim();
            if (!IsTeam(Name, login)){
                Team Team_ = new Team();
                Team_.Name = Name;
                Team_.Leader = UserService_.GetIdUser(login);//id лидера команды
                Company company = UserService_.GetCompany(login);
                if (company != null)
                {
                    Team_.IdCompany = company.Id;
                }
                Context.Team.Add(Team_);
                Context.SaveChanges();
                ClientToDo clientToDo = UserService_.GetUser(login);
                clientToDo.IdTeam = Team_.Id;
                Context.SaveChanges();
                return true;
           }
           return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCompany"></param>
        /// <returns></returns>
        public Team GetTeamById(int idCompany)
        {
            return Context.Team.Find(idCompany);
        }
    }
}
