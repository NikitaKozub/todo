﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;
using ToDo2.Models.Service;

namespace ToDo2.Models.Service
{
    public class CommentService
    {
        ToDoContext Context;
        UserService UserService_;
        public CommentService(ToDoContext context)
        {
            Context = context;
            UserService_ = new UserService(Context); ;
        }

        /// <summary>
        /// Добавить коммент(если задача не твоя или лидеру группы, то не довиться)
        /// </summary>
        /// <param name="text">текст</param>
        /// <param name="login">логин</param>
        /// <param name="IdCase">id задачи</param>
        /// <returns>добавил или нет</returns>
        public bool AddComment(string text, string login, string IdCase)
        {
            int idCase = Int16.Parse(IdCase);
            bool isCaseTrue = IsCaseUser(login, idCase);
            ClientToDo clientTo = UserService_.GetUser(login);
            if (isCaseTrue)
            {
                Comment comment = new Comment()
                {
                    Text = text,
                    IdCase = idCase,
                    IdUser = clientTo.Id
                };
                Context.Comment.Add(comment);
                Context.SaveChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Проверяет принадлежит ли задание пользователю или он является лидером
        /// </summary>
        /// <param name="loginUser">логин пользователя</param>
        /// <param name="idCase">id задания</param>
        /// <returns></returns>
        public bool IsCaseUser(string loginUser, int idCase)
        {
            Case case_ = Context.Case.Find(idCase);
            ClientToDo clientToDo = UserService_.GetUser(loginUser);

            if (case_.IdUser == clientToDo.Id || case_.IdLeaderUser == clientToDo.Id)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Удалить комментарий по id задания
        /// </summary>
        /// <param name="idCase">id задания</param>
        public void DeleteComment(int idCase)
        {
            var comments = Context.Comment.Where(c => c.IdCase == idCase);
            Context.RemoveRange(comments);
            Context.SaveChanges();
        }

        /// <summary>
        /// Получить данные для отображения на странице 
        /// CommentCase.cshtml(само задание и комменты к нему)
        /// </summary>
        /// <param name="id">номер задания</param>
        /// <param name="Comments">все комменты задания</param>
        /// <returns>задание</returns>
        public Case GetCaseForCommentView(int id, out List<Comment> Comments)
        {
            CaseService CaseService_ = new CaseService(Context);
            Case currentCase = CaseService_.GetCase(id);

            currentCase.IdUserNavigation = UserService_.GetUser(currentCase.IdUser);
            Comments = Context.Comment.Where(c => c.IdCase == currentCase.Id).ToList();
            foreach (var itr in Comments)
            {
                itr.IdUserNavigation = UserService_.GetUser((int)itr.IdUser);
            }
            if (Comments == null)
            {
                Comments = new List<Comment>();
            }
            return currentCase;
        }
    }
}
