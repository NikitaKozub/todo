﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;

namespace ToDo2.Models.Service
{
    public class CompanyService
    {
        ToDoContext Context;
        
        public CompanyService(ToDoContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Добавляет комапнию
        /// </summary>
        /// <param name="name">название компании</param>
        /// <returns>если компания есть - возвращает ее, если нет - возвращает null</returns>
        public Company AddCompany(string name)
        {
            Company Company = IsCompany(name);
            if (Company == null){
                Company company = new Company();
                company.Name = name;
                Context.Company.Add(company);
                Context.SaveChanges();
                return company;
            }
            return Company;
        }

        /// <summary>
        /// Проверка есть ли компания
        /// </summary>
        /// <param name="name">название</param>
        /// <returns>есть - возвращает объект, нет - возвращает null</returns>
        private Company IsCompany(string name)
        {
            Company company = Context.Company.FirstOrDefault(c => c.Name == name);
            return company;
        }
    }
}
