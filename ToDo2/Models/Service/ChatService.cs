﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDo2.DataBase;
using ToDo2.Models.Service;

namespace ToDo2.Models.Service
{
    public class ChatService
    {
        ToDoContext Context;
        UserService UserService_;
        TeamService TeamService_;
        public ChatService(ToDoContext context)
        {
            Context = context;
            UserService_ = new UserService(Context);
            TeamService_ = new TeamService(Context);
        }

        /// <summary>
        /// Получить все сообщения чата для команды
        /// </summary>
        /// <param name="idTeam">id команды</param>
        /// <returns>Лист с сообщениями</returns>
        public List<ToDo2.DataBase.Chat> GetChatMessages(int idTeam)
        {
            var listChatMessages = new List<ToDo2.DataBase.Chat>();
            listChatMessages = Context.Chat.Where(c => c.IdTeam == idTeam).ToList();
            return listChatMessages;
        }

        /// <summary>
        /// Добавить в чат сообщение
        /// </summary>
        /// <param name="chat"></param>
        /// <returns></returns>
        public bool AddChatMessage(Message_ chat)
        {
            ToDo2.DataBase.Chat chatMessage_ = new ToDo2.DataBase.Chat();

            chatMessage_.IdUser = (int)UserService_.GetIdUser(chat.LoginUser);
            chatMessage_.IdTeam = TeamService_.GetTeam(chat.LoginUser).Id;
            if (chatMessage_.IdUser != null || chatMessage_.IdTeam != null)
            {
                ToDo2.DataBase.Chat ChatMessage = new ToDo2.DataBase.Chat()
                {
                    IdTeam = chatMessage_.IdTeam,
                    IdUser = chatMessage_.IdUser,
                    Text = chat.Text
                };
                Context.Chat.Add(ChatMessage);
                Context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
