﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;

namespace ToDo2.Models.Service
{
    public class CaseService
    {
        ToDoContext Context;
        UserService UserService_;
        TeamService TeamService_;
        CommentService CommentService_;
        public CaseService(ToDoContext context)
        {
            Context = context;
            UserService_ = new UserService(Context);
            TeamService_ = new TeamService(Context);
            CommentService_ = new CommentService(Context);
        }

        /// <summary>
        /// Получить данные для case.cshtml
        /// </summary>
        /// <param name="login">login</param>
        /// <returns>список заданий пользователя</returns>
        public List<Case> GetCasesView(string login)
        {
            List<Case> Cases = GetCases(login);
            foreach (var itr in Cases)
            {
                itr.IdUserNavigation = UserService_.GetUser(itr.IdUser);
            }
            return Cases;
        }

        /// <summary>
        /// Получить все командные задания
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="team">команда</param>
        /// <param name="YouCases">твои задания</param>
        /// <param name="client">ты</param>
        /// <param name="UsersTeam">список команды</param>
        public void GetCasesTeamView(string login,
            out Team team, out List<Case> YouCases,
            out ClientToDo client, out List<ClientToDo> UsersTeam)
        {
            client = UserService_.GetUser(login);
            team = TeamService_.GetTeam(login);
            YouCases = new List<Case>();
            if (team != null)
            {
                YouCases = GetYourTeamTasks(UserService_.GetIdUser(login), team.Id);
                foreach (var itr in YouCases)
                {
                    itr.IdUserNavigation = UserService_.GetUser(itr.IdUser);
                }
            }
            UsersTeam = TeamService_.GetUserTeam(login);
        }


        /// <summary>
        /// Добавить задание
        /// </summary>
        /// <param name="model">viewmodel окна</param>
        /// <param name="login">имя пользователя</param>
        public bool AddCase(string Start, string End, string Title, string Description, string login)
        {
            try 
            { 
                DateTime start = Convert.ToDateTime(Start);
                DateTime end = Convert.ToDateTime(End);
                if (start > end) {
                    return false;
                }
                Case case_ = new Case()
                {
                    Title = Title,
                    Start = start,
                    End = end,
                    Description = Description,
                    Private = true,
                    IsComplete = StatusCase.В_процессе.ToString(),
                    IdUser = UserService_.GetIdUser(login)
                };
                Context.Case.Add(case_);
                Context.SaveChanges();
                return true;
            }
            catch (DbUpdateException e)
            {
                //Дубль
                return true;
            }
        }
        
        /// <summary>
        /// Выполнить задание
        /// </summary>
        /// <param name="id"></param>
        public void CompleteCase(int id)
        {
            Context.Case.FirstOrDefault(c => c.Id == id).IsComplete = StatusCase.Сделано.ToString(); ;
            Context.SaveChanges();
        }

        /// <summary>
        /// Принять задание
        /// </summary>
        /// <param name="id"></param>
        public void AcceptedCase(int id)
        {
            Context.Case.FirstOrDefault(c => c.Id == id).IsComplete = StatusCase.Принято.ToString();
            Context.SaveChanges();
        }

        /// <summary>
        /// Добавить групповое задание
        /// </summary>
        /// <param name="model">viewmodel окна</param>
        /// <param name="login">имя пользователя создающего задания</param>
        public bool AddCaseTeam(string Start, string End, string Title, string Description, int IdUserTeam, string login)
        {
            try
            {
                DateTime start = Convert.ToDateTime(Start);
                DateTime end = Convert.ToDateTime(End);
                if (start > end)
                {
                    return false;
                }
                Case case_ = new Case()
                {
                    Title = Title,
                    Start = start,
                    End = end,
                    Description = Description,
                    Private = false,
                    IsComplete = StatusCase.В_процессе.ToString(),
                    IdUser = IdUserTeam,
                    IdLeaderUser = UserService_.GetIdUser(login)
                };
                Context.Case.Add(case_);
                Context.SaveChanges();
                ClientToDo client = Context.ClientToDo.First(c => c.Id == case_.IdUser);
                Team team = TeamService_.GetTeam(case_.IdUser);// Context.Team.First(c => c.Id == client.IdTeam);
                TeamCase teamCase = new TeamCase();
                teamCase.IdCase = case_.Id;
                teamCase.IdTeam = (int)client.IdTeam;
                Context.TeamCase.Add(teamCase);
                Context.SaveChanges();
                return true;
            }
            catch (DbUpdateException e)
            {
                //Дубль
                return true;
            }
        }        

        /// <summary>
        /// Отменить задание
        /// </summary>
        /// <param name="_case">задание</param>
        public void CancelCase(int id)
        {
            Case _case = Context.Case.FirstOrDefault(c => c.Id == id);
            CommentService_.DeleteComment(_case.Id);
            Context.Case.Remove(_case);
            Context.SaveChanges();
        }

        /// <summary>
        /// Отменить командное задание
        /// </summary>
        /// <param name="_case">задание</param>
        public void CancelCaseTeam(int id)
        {
            Case _case = Context.Case.FirstOrDefault(c => c.Id == id);
            TeamCase teamCase = Context.TeamCase.FirstOrDefault(t => t.IdCase == _case.Id);
            CommentService_.DeleteComment(_case.Id);
            if (teamCase != null)
            {
                Context.TeamCase.Remove(teamCase);
            }
            Context.Case.Remove(_case);
            Context.SaveChanges();
        }

        /// <summary>
        /// Получить все задания пользователя
        /// </summary>
        /// <param name="name">Имя пользователя, чьи задания надо вернуть</param>
        /// <returns></returns>
        public List<Case> GetCases(string login)
        {
            ClientToDo user = UserService_.GetUser(login);
            List<Case> cases = Context.Case.Where(c => c.IdUser == user.Id && c.Private == true).ToList();
            if (cases == null)
            {
                cases = new List<Case>();
            }

            return cases;
        }

        /// <summary>
        /// Получить все задания команды
        /// </summary>
        /// <param name="name">Имя пользователя</param>
        /// <returns></returns>
        public List<Case> GetTeamCases(string login)
        {
            ClientToDo user = UserService_.GetUser(login);
            Team team = TeamService_.GetTeam(user.Id);// Context.Team.FirstOrDefault(t => t.Id == user.IdTeam);
            List<Case> casesTeam = new List<Case>();
            if (team != null)
            {
                var teamCases = Context.TeamCase.Where(c => c.IdTeam == team.Id).Select(x => x.IdCase).ToList();
                casesTeam = Context.Case.AsNoTracking().Where(s => teamCases.Contains(s.Id)).ToList();
                return casesTeam;
            }

            return casesTeam;
        }

        /// <summary>
        /// Получить все задания пользователя находящегося в команде
        /// </summary>
        /// <param name="nameUser">Имя пользователя</param>
        /// <param name="nameCompany">Название компании</param>
        /// <returns>Список заданий</returns>
        public List<Case> GetYourTeamTasks(int idUser, int idCompany)
        {
            List<Case> casesTeam = new List<Case>();
            ClientToDo user = Context.ClientToDo.FirstOrDefault(u => u.Id == idUser);
            Team team = TeamService_.GetTeamById(idCompany); //Context.Team.FirstOrDefault(t => t.Id == idCompany);
            if (team != null)
            {
                var teamCases = Context.TeamCase.Where(c => c.IdTeam == team.Id).Select(x => x.IdCase).ToList();
                casesTeam = Context.Case.AsNoTracking().Where(s => teamCases.Contains(s.Id) && s.IdUser==user.Id).ToList();
                return casesTeam;
            }

            return casesTeam;
        }

        /// <summary>
        /// Отметить задание, как проваленое
        /// </summary>
        /// <param name="id">номер задания</param>
        public void FailedCaseTeam(int id)
        {
            Context.Case.FirstOrDefault(c => c.Id == id).IsComplete = StatusCase.Провалено.ToString();
            Context.SaveChanges();
        }

        /// <summary>
        /// Отметить задания, как подтвержденное
        /// </summary>
        /// <param name="id">номер задания</param>
        public void CaseConfirmed(int id)
        {
            Context.Case.FirstOrDefault(c => c.Id == id).IsComplete = StatusCase.Подтверждено.ToString();
            Context.SaveChanges();
        }

        /// <summary>
        /// Получить задание по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Case GetCase(int id)
        {
            return Context.Case.SingleOrDefault(c => c.Id == id);
        }
    }
}
