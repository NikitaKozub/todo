﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;

namespace ToDo2.Models.Service
{
    public class UserService
    {
        ToDoContext Context;
        private int USER_ROLE = 2;// В базе данных роле пользователя под 2 id

        public UserService(ToDoContext context)
        {
            Context = context;
        }

        public void PersonalAreaView(TeamService teamService,string login_,
            out string name, out string email, out string login, out string password,
            out string company_, out string team_)
        {
            ClientToDo user_ = GetUser(login_);
            Credentials credentials = GetCredentials(login_);
            Company company = GetCompany(login_);
            Team team = teamService.GetTeam(login_);
            name = user_.Name;
            email = user_.Email;
            login = credentials.Login;
            password = credentials.Password;
            company_ = company == null ? "Вы не писали компанию" : company.Name;
            team_ = team == null ? "Вы не состоите в группе" : team.Name;
        }

        /// <summary>
        /// Добавить пользователя
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id_company">id компании, компания не обязательна</param>
        public bool AddUser(string emailAsLogin,string password, Company company)
        {
            int? id_company;
            if (company == null)
            {
                id_company = null;
            }
            else
            {
                id_company = company.Id;
            }
            if (GetIdUser(emailAsLogin) == -1)
            {
                Credentials credentials = new Credentials();
                credentials.Login = emailAsLogin;
                credentials.Password = password;
                Context.Credentials.Add(credentials);
                Context.SaveChanges();

                ClientToDo clientToDo = new ClientToDo
                {
                    Name = emailAsLogin,
                    Email = emailAsLogin,
                    IdCrendentials = credentials.Id,
                    IdCompany = id_company
                };
                Context.ClientToDo.Add(clientToDo);
                Context.SaveChanges();
                UserRole userRole = new UserRole();
                userRole.RoleId = USER_ROLE;
                userRole.UserId = clientToDo.Id;
                Context.UserRole.Add(userRole);
                Context.SaveChanges();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Возвращает id пользователя
        /// </summary>
        /// <param name="name">логин</param>
        /// <returns>id ClientToDo</returns>
        public int GetIdUser(string login)
        {
            ClientToDo user = GetUser(login);
            if (user != null)
            {
                return user.Id;
            }
            return -1;
        }

        /// <summary>
        /// Получить пользователя
        /// </summary>
        /// <param name="name">Имя пользователя</param>
        /// <returns>Объект ClientToDo</returns>
        public ClientToDo GetUser(string login)
        {
            Credentials credentials = Context.Credentials.FirstOrDefault(c => c.Login == login);
            if (credentials == null)
            {
                return null;
            }
            ClientToDo user = Context.ClientToDo.FirstOrDefault(u => u.IdCrendentials == credentials.Id);
            return user;
        }

        /// <summary>
        /// Получить пользователя
        /// </summary>
        /// <param name="id">id пользователя</param>
        /// <returns>Объект ClientToDo</returns>
        public ClientToDo GetUser(int id)
        {
            ClientToDo user = Context.ClientToDo.FirstOrDefault(c => c.Id == id);
            return user;
        }

        /// <summary>
        /// Получить пользоватлей в компании, без команды
        /// </summary>
        /// <param name="nameCompany">имя компании</param>
        /// <returns></returns>
        public List<ClientToDo> GetUsersCompanyWithoutTeam(string nameCompany)
        {
            Company company = Context.Company.FirstOrDefault(c => c.Name == nameCompany);
            List<ClientToDo> users = Context.ClientToDo.Where(c => c.IdCompany == company.Id && c.IdTeam == null).ToList();
            if (users == null)
            {
                users = new List<ClientToDo>();
            }
            return users;
        }

        /// <summary>
        /// Получить логин и пароль
        /// </summary>
        /// <param name="login">логин</param>
        /// <returns></returns>
        public Credentials GetCredentials(string login)
        {
            Credentials credentials = Context.Credentials.FirstOrDefault(c => c.Login == login);
            return credentials;
        }

        /// <summary>
        /// Получить компанию пользователя
        /// </summary>
        /// <param name="name">имя пользователя</param>
        /// <returns>Объект company</returns>
        public Company GetCompany(string login)
        {
            ClientToDo user = GetUser(login);
            if (user.IdCompany == null)
            {
                return null;
            }
            Company company = Context.Company.FirstOrDefault(c => c.Id == user.IdCompany);
            return company;
        }

        /// <summary>
        /// Обновление личныйх данных
        /// </summary>
        /// <param name="model">модель с данными пользователя</param>
        /// <param name="login">логин пользователя</param>
        public bool UpdatePersonalData(string Name, string Email,string updateLogin,string Password, string login)
        {
            ClientToDo user_ = GetUser(login);
            user_.Name = Name;
            user_.Email = Email;
            Credentials credentials;
            if ((credentials = Context.Credentials.FirstOrDefault(c => c.Login == updateLogin)) != null)
            {
                credentials = Context.Credentials.FirstOrDefault(c => c.Id == user_.IdCrendentials);
                credentials.Login = login;
            }
            else
            {
                credentials = Context.Credentials.FirstOrDefault(c => c.Id == user_.IdCrendentials);
                credentials.Login = updateLogin;
            }
            credentials.Password = Password;
            Context.SaveChanges();    
            return true;
        }
    }
}
