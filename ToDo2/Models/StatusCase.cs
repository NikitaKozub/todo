﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDo2.Models
{
    public enum StatusCase
    {
        Сделано,
        Провалено,
        В_процессе,
        Подтверждено,
        Принято
    }
}
