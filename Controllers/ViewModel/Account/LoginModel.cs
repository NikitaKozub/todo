﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Controllers.ViewModel
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Не указан Login")]
        [DataType(DataType.Text)]
        [StringLength(32, MinimumLength = 5, ErrorMessage = "Минимум 5 символов")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        [StringLength(32, MinimumLength = 5, ErrorMessage = "Минимум 5 символов")]
        public string Password { get; set; }
    }
}
