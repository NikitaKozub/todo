﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;

namespace Controllers.ViewModel
{
    public class CommentViewModel
    {
        /// <summary>
        /// Задания к которому будет комментарии
        /// </summary>
        public Case Case { get; set; }

        /// <summary>
        /// Комментарии
        /// </summary>
        public List<Comment> Comments { get; set; }

        /// <summary>
        /// Ваш комментарий
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// id задания которое комментируется
        /// </summary>
        public int IdCase { get; set; }
    }
}
