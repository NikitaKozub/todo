﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;

namespace Controllers.ViewModel.Team_
{
    public class ManagerTeamViewModel
    {
        /// <summary>
        /// Задания команды
        /// </summary>
        public IEnumerable<Case> TeamCases { get; set; }
    }
}
