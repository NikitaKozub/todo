﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;

namespace Controllers.ViewModel
{
    public class PersonalAreaViewModel
    {
        [Required]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}", ErrorMessage = "Некорректный адрес")]
        public string Email { get; set; }

        public string Company { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        [StringLength(32, MinimumLength = 5, ErrorMessage = "Минимум 5 символов")]
        public string Password { get; set; }

        [Required]
        [StringLength(32, MinimumLength = 5, ErrorMessage = "Минимум 5 символов")]
        public string Name { get; set; }
        
        public string Team { get; set; }
        [Required]
        [StringLength(32, MinimumLength = 5, ErrorMessage = "Минимум 5 символов")]
        public string Login { get; set; }

        public bool IsLeader { get; set; }
    }
}
