﻿using System;
using System.ComponentModel.DataAnnotations;
using ToDo2.DataBase;

namespace Controllers.ViewModel.Board
{
    /// <summary>
    /// Общие детали задания
    /// </summary>
    public class AbstractBoard
    {
        /// <summary>
        /// Название задания
        /// </summary>
        private string title;
        //[Required(ErrorMessage = "Не указано название или слишком длинное")]
        [DataType(DataType.Text)]
        [StringLength(32, MinimumLength = 5, ErrorMessage = "Минимум 5 символов")]
        public string Title 
        {
            get
            {
                return title;
            }
            set
            {
                title = value.Trim();
            }
        }

        /// <summary>
        /// Начало задания
        /// </summary>
        [Required(ErrorMessage = "Не указана дата")]
        [DataType(DataType.DateTime)]
        public string Start { get; set; }

        /// <summary>
        /// Конец задания
        /// </summary>
        [Required(ErrorMessage = "Не указана дата")]
        [DataType(DataType.DateTime)]
        public string End { get; set; }

        /// <summary>
        /// Описание задания
        /// </summary>
        private string description;
        [Required(ErrorMessage = "Не указано описание или слишком длинное")]
        [DataType(DataType.Text)]
        [StringLength(50, MinimumLength = 5,ErrorMessage = "Минимум 5 символов")]
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value.Trim();
            }
        }

        /// <summary>
        /// Выполнено ли
        /// </summary>
        [StringLength(12)]
        public string IsComplete { get; set; }
        
        /// <summary>
        /// Если вы одни true, если вы в команде false
        /// </summary>
        public bool Private { get; set; }

        /// <summary>
        /// Наличие команды, если есть будет объект, иначе null
        /// </summary>
        public Team Team { get; set; }
    }
}
