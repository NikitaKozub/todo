﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;

namespace Controllers.ViewModel.Board
{
    public class BoardTeamViewModel : AbstractBoard
    {
        /// <summary>
        /// Авторизованный пользователь
        /// </summary>
        public ClientToDo User { get; set; }

        /// <summary>
        /// id пользователя команды
        /// </summary>
        public int IdUserTeam { get; set; }

        /// <summary>
        /// Твои задания в команде
        /// </summary>
        public IEnumerable<Case> YouCases { get; set; }

        /// <summary>
        /// Пользователи состоящие в команде
        /// </summary>
        public IEnumerable<ClientToDo> UsersTeam { get; set; }
    }
}
