﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;

namespace Controllers.ViewModel.Board
{
    public class BoardViewModel : AbstractBoard
    {
        /// <summary>
        /// Задания
        /// </summary>
        public IEnumerable<Case> Cases { get; set; }
    }
}
