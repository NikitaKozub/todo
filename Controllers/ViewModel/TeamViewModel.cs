﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;

namespace Controllers.ViewModel.Team_
{
    public class TeamViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Не указано название")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        /// <summary>
        /// Команды компании
        /// </summary>
        public List<Team> CompanyTeams { get; set; }

        /// <summary>
        /// Команды не входящие в какую либо компанию
        /// </summary>
        public List<Team> TeamWithoutCompany { get; set; }

        /// <summary>
        /// Твоя команда
        /// </summary>
        public Team YouTeam { get; set; }

        /// <summary>
        /// Компания в которой работаешь
        /// </summary>
        public Company YouCompany { get; set; }
        
        /// <summary>
        /// Пользователи состоящие в компании, не состоящие в команде
        /// </summary>
        public IEnumerable<ClientToDo> UsersInCompanyWithoutTeam { get; set; }

        /// <summary>
        /// Пользователи состоящие в компании, состоящие в команде
        /// </summary>
        public IEnumerable<ClientToDo> UserInCompanyInTeam { get; set; }

        /// <summary>
        /// Пользователи не состоящие в компании, не состоящие в команде
        /// </summary>
        public IEnumerable<ClientToDo> UsersWithoutCompanyWithoutTeam { get; set; }

        /// <summary>
        /// Пользователи не состоящие в компании, состоящие в команде
        /// </summary>
        public IEnumerable<ClientToDo> UsersWithoutCompanyInTeam { get; set; }

        /// <summary>
        /// Является ли лидером
        /// </summary>
        public bool IsLeader { get; set; }
    }
}
