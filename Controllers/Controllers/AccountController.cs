﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ToDo2.DataBase;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ToDo2.Models.Service;
using Controllers.ViewModel;

namespace Controllers.Controllers
{
    public class AccountController : Controller
    {
        private ToDoContext db;
        public RegisterViewModel model;
        public UserService UserService_;
        public CompanyService CompanyService_;
        public AccountController(ToDoContext context)
        {
            db = context;
            UserService_ = new UserService(db);
            CompanyService_ = new CompanyService(db);
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(ViewModel.LoginModel model)
        {
            if (ModelState.IsValid)
            {

                ClientToDo user_;
                Credentials credentials = await db.Credentials
                    .FirstOrDefaultAsync(u => u.Login == model.Login && u.Password == model.Password);
                if (credentials == null)
                {
                    ModelState.AddModelError("Login", "Данные не верны");
                    ModelState.AddModelError("Password", "Данные не верны");
                    return View();
                }
                else
                {
                    user_ = await db.ClientToDo
                     .FirstOrDefaultAsync(u => u.IdCrendentials == credentials.Id);
                }

                await Authenticate(model.Login); // аутентификация
                return RedirectToAction("Case", "Home");
            }
            return View();
        }

        [HttpGet]
        public IActionResult Register()
        {
            model = new RegisterViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                Company currentcompany;
                if(model.Company == null)
                {
                    currentcompany = null;
                }
                else
                {
                    currentcompany = CompanyService_.AddCompany(model.Company);
                }

                if (UserService_.AddUser(model.Email, model.Password, currentcompany))
                {
                    await Authenticate(model.Email); // аутентификация
                    return RedirectToAction("Case", "Home");
                }
                else
                {
                    ModelState.AddModelError("Email", "Такой пользователь уже есть");
                }
            }
            return View();
        }

        private async Task Authenticate(string userName)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, userName)
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
    }
}