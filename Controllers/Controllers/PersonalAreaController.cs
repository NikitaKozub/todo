﻿using Controllers.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;
using ToDo2.Models.Service;

namespace Controllers.Controllers
{
    public class PersonalAreaController : Controller
    {
        private ToDoContext Context;
        public UserService UserService_;
        public TeamService TeamService;
        public PersonalAreaViewModel PersonalAreaViewModel_;
        public PersonalAreaController(ToDoContext context)
        {
            Context = context;
            UserService_ = new UserService(Context);
            TeamService = new TeamService(Context); 
        }

        [Authorize]
        [HttpGet]
        public IActionResult PersonalArea()
        {
            string name;
            string email;
            string login;
            string password;
            string company_;
            string team_;
            UserService_.PersonalAreaView(TeamService, User.Identity.Name,
            out name, out email, out login, out password, out company_, out team_);
            ViewBag.name = name;
            ViewBag.email = email;
            ViewBag.login = login;
            ViewBag.password = password;
            ViewBag.company_ = company_;
            ViewBag.team_ = team_;
            return View();
        }

        /// <summary>
        /// Обновление данных
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult PersonalArea(PersonalAreaViewModel model)
        {
            if (ModelState.IsValid)
            {
                if(UserService_.UpdatePersonalData(model.Name, model.Email, 
                    model.Login, model.Password, User.Identity.Name))
                {
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    return View();
                }
                
            }
            return PersonalArea();
        }
    }
}
