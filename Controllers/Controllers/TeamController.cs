﻿using Controllers.ViewModel.Team_;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using ToDo2.DataBase;
using ToDo2.Models.Service;

namespace Controllers.Controllers
{
    public class TeamController : Controller
    {
        private ToDoContext Context;
        public CaseService CaseService_;
        public UserService UserService_;
        public TeamService TeamService_;
        public TeamViewModel model;
        public TeamController(ToDoContext context)
        {
            Context = context;
            CaseService_ = new CaseService(Context);
            TeamService_ = new TeamService(Context);
            UserService_ = new UserService(Context);
        }

        [Authorize]
        [HttpGet]
        public IActionResult Team()
        {
            ChatService chatService = new ChatService(Context);
            Team YouTeam; Company YouCompany; bool IsLeader;
            List<ClientToDo> UsersWithoutCompanyInTeam; List<Team> TeamWithoutCompany;
            List<ClientToDo> UserInCompanyInTeam; List<ClientToDo> UsersInCompanyWithoutTeam;
            List<Team> CompanyTeams; List<Chat> ListChatMessage;
            TeamService_.GetTeamView(User.Identity.Name, chatService,
            out YouTeam, out YouCompany, out IsLeader,
            out UsersWithoutCompanyInTeam, out TeamWithoutCompany,
            out UserInCompanyInTeam, out UsersInCompanyWithoutTeam,
            out CompanyTeams, out ListChatMessage);

            ViewBag.YouTeam = YouTeam;
            ViewBag.YouCompany = YouCompany;
            ViewBag.IsLeader = IsLeader;
            ViewBag.UsersWithoutCompanyInTeam = UsersWithoutCompanyInTeam;
            ViewBag.TeamWithoutCompany = TeamWithoutCompany;
            ViewBag.UserInCompanyInTeam = UserInCompanyInTeam;
            ViewBag.UsersInCompanyWithoutTeam = UsersInCompanyWithoutTeam;
            ViewBag.CompanyTeams = CompanyTeams;
            ViewBag.ListChatMessage = ListChatMessage;
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Team(TeamViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (TeamService_.AddTeam(model.Name, User.Identity.Name))
                {
                    return RedirectToAction(nameof(Team));
                }
                else
                {
                    ModelState.AddModelError("Name", "Команда с подобным названием уже существует");
                    return Team();
                } 
            }
            return RedirectToAction(nameof(Team));
        }
    }
}
