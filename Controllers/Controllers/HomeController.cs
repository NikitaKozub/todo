﻿using System.Collections.Generic;
using System.Diagnostics;
using Controllers.ViewModel.Board;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ToDo2;
using ToDo2.DataBase;
using ToDo2.Models.Service;

namespace Controllers.Controllers.Board
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private ToDoContext db;
        public List<Case> Cases;
        CaseService CaseService_;
        TeamService TeamService_;
        UserService UserService_;
        public HomeController(ILogger<HomeController> logger, ToDoContext context)
        {
            _logger = logger;
            db = context;
            CaseService_ = new CaseService(db);
            TeamService_ = new TeamService(db);
            UserService_ = new UserService(db);
        }

        [Authorize]
        [HttpGet]
        public IActionResult Case()
        {
            ViewBag.Cases = CaseService_.GetCasesView(User.Identity.Name);
            return View();
        }          


        /// <summary>
        /// Добавление задания
        /// </summary>
        /// <param name="model">данные из окнам</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Case(BoardViewModel model)
        {
            if (ModelState.IsValid)
            {
                if(!CaseService_.AddCase(model.Start, model.End, model.Title, model.Description, User.Identity.Name))
                {
                    ModelState.AddModelError("End", "Даты начала позже даты конца");
                    return Case();
                }
                return RedirectToAction(nameof(Case));
            }
            return Case();
        }

        [Authorize]
        public IActionResult CaseTeam()
        {
            Team Team;
            List<Case> YouCases;
            ClientToDo ClientToDo;
            List<ClientToDo> UsersTeam;
            CaseService_.GetCasesTeamView(User.Identity.Name,
                out Team, out YouCases, out ClientToDo, out UsersTeam);

            ViewBag.Team = Team;
            ViewBag.YouCases = YouCases;
            ViewBag.ClientToDo = ClientToDo;
            ViewBag.UsersTeam = UsersTeam;

            return View("CaseTeam");
        }

        /// <summary>
        /// Добавления задания в команде
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CaseTeam(BoardTeamViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!CaseService_.AddCaseTeam(model.Start, model.End, model.Title, model.Description, model.IdUserTeam, User.Identity.Name))
                {
                    ModelState.AddModelError("End", "Даты начала позже даты конца");
                    return CaseTeam();
                }
                return CaseTeam();
            }
            return CaseTeam();
        }

        public IActionResult AddComment(int id)
        {
            return RedirectToAction("AddComment", "Comment", new { Id = id });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
