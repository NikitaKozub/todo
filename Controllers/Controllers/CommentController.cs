﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;
using ToDo2.Models.Service;

namespace Controllers.Controllers
{
    public class CommentController : Controller
    {
        private readonly ILogger<CommentController> _logger;
        private ToDoContext Context;
        private CommentService CommentService;
        public CommentController(ILogger<CommentController> logger, ToDoContext context)
        {
            _logger = logger;
            Context = context;
            CommentService = new CommentService(Context);
        }

        [Authorize]
        public IActionResult CommentCase(int id)
        {
            List<Comment> Comments;
            ViewBag.currentCase = CommentService.GetCaseForCommentView(id, out Comments);
            ViewBag.Comments = Comments; 
            return View();
        }
    }
}
