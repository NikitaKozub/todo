﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using ToDo2.DataBase;
using ToDo2.Models.Service;

namespace Controllers.Controllers
{
    public class ManagerTeamController : Controller
    {
        private readonly ILogger<ManagerTeamController> _logger;
        private ToDoContext db;
        public ManagerTeamController(ILogger<ManagerTeamController> logger, ToDoContext context)
        {
            _logger = logger;
            db = context;
        }

        [Authorize]
        [HttpGet]
        public IActionResult ManagerTeam()
        {
            TeamService teamServiece = new TeamService(db);
            List<Case> TeamCases;
            Team team; 
            List<Chat> ListChatMessage;
            teamServiece.GetManagerTeamView(User.Identity.Name, out TeamCases,
            out team, out ListChatMessage);
            ViewBag.TeamCases = TeamCases;
            ViewBag.team = team;
            ViewBag.ListChatMessage = ListChatMessage;
            return View();
        }
    }
}
