﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System.Security.Claims;
using System;
using System.Threading.Tasks;
using ToDo2.DataBase;
using ToDo2.Models.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using ToDo2.Models;

namespace Controllers.Hub_
{
    public class MessageHub : Hub
    {
        ChatService ChatService_;
        
        CommentService commentService;
        
        ToDoContext ContextToDo = new ToDoContext();
        TeamService teamService ;
        public override async Task OnConnectedAsync()
        {
            teamService = new TeamService(ContextToDo);
            await Groups.AddToGroupAsync(Context.ConnectionId, teamService.GetTeam(Context.User.Identity.Name).Name);
            await base.OnConnectedAsync();
        }

        /// <summary>
        /// Пересылает сообщения чата
        /// </summary>
        /// <param name="chat"></param>
        /// <returns></returns>
        [Authorize]
        public async Task SendChatMessage(Message_ chat)
        {
            ChatService_ = new ChatService(ContextToDo);
            teamService = new TeamService(ContextToDo);
            string name = Context.User.Identity.Name;
            string group = teamService.GetTeam(name).Name;
            chat.LoginUser = name;

            if (ChatService_.AddChatMessage(chat))
            {
                await Clients.Group(group).SendAsync("chatMessage", chat);
            }
        }

        /// <summary>
        /// Пересылает сообщения в комментариях
        /// </summary>
        /// <param name="comment"></param>
        /// <returns></returns>
        [Authorize]
        public async Task SendCommentMessage(MessageComment comment)
        {
            commentService = new CommentService(ContextToDo);
            teamService = new TeamService(ContextToDo);
            string name = Context.User.Identity.Name;
            string group = teamService.GetTeam(name).Name;
            comment.LoginUser = name;

            if (commentService.AddComment(comment.Text, name, comment.IdCase))
            {
                await Clients.Group(group).SendAsync("commentMessage", comment);
            }
        }
    }
}
