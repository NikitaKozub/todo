﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;
using ToDo2.Models.Service;

namespace Controllers.Hub_
{
    public class TeamHub : Hub
    {
        ToDoContext ContextToDo = new ToDoContext();
        TeamService teamService;
        UserService userService;

        public override async Task OnConnectedAsync()
        {
            teamService = new TeamService(ContextToDo);
            var team = teamService.GetTeam(Context.User.Identity.Name);
            if (team != null)
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, teamService.GetTeam(Context.User.Identity.Name).Name);
                await base.OnConnectedAsync();
            }
        }

        /// <summary>
        /// Удаление члена команды
        /// </summary>
        /// <param name="memberTeam"></param>
        /// <returns></returns>
        [Authorize]
        public async Task SendDeleteMemeberTeam(MemberTeam<int> memberTeam)
        {
            teamService = new TeamService(ContextToDo);
            userService = new UserService(ContextToDo);
            string name = Context.User.Identity.Name;
            string group = teamService.GetTeam(name).Name;
            ClientToDo user =  userService.GetUser(memberTeam.NameUser);
            bool isLeader = teamService.DeleteTeamMember(user.Id);
            if (isLeader){
                await Clients.Group(group).SendAsync("deleteLeaderMember", memberTeam);
            }
            else
            {
                await Clients.Group(group).SendAsync("deleteMember", memberTeam);
            }
        }

        [Authorize]
        public async Task JT(string IdTeam)
        {
            teamService = new TeamService(ContextToDo);
            userService = new UserService(ContextToDo);
            ClientToDo user = userService.GetUser(Context.User.Identity.Name);
            teamService.JoinTeam(Int32.Parse(IdTeam), user.Name);
            await Clients.Caller.SendAsync("joinTeam", IdTeam);
            string group = teamService.GetTeam(user.Name).Name;
            var memberTeam = new MemberTeam<int>() { NameUser = user.Name, IdSelected = user.Id };
            await Clients.OthersInGroup(group).SendAsync("joinMember", memberTeam);
        }
    }

    public class MemberTeam<I>
    {
        public string NameUser { get; set; }
        public I IdSelected { get; set; }
    }
    public class Team
    {
        public string IdTeam { get; set; }
    }
}
