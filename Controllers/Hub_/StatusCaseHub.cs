﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using ToDo2.DataBase;
using ToDo2.Models.Service;

namespace Controllers.Hub_
{
    public class StatusCaseHub : Hub
    {
        ToDoContext ContextToDo = new ToDoContext();
        TeamService teamService;

        public override async Task OnConnectedAsync()
        {
            teamService = new TeamService(ContextToDo);
            await Groups.AddToGroupAsync(Context.ConnectionId, teamService.GetTeam(Context.User.Identity.Name).Name);
            await base.OnConnectedAsync();
        }

        /// <summary>
        /// Смена статуса задания
        /// </summary>
        /// <param name="imgStatus_"></param>
        /// <returns></returns>
        [Authorize]
        public async Task ChangeStatusCase(ImgStatus imgStatus_)
        {
            CaseService caseService = new CaseService(ContextToDo);
            teamService = new TeamService(ContextToDo);
            string name = Context.User.Identity.Name;
            string group = teamService.GetTeam(name).Name;
            
            int id_ = Convert.ToInt32(imgStatus_.IdCase);
            switch (imgStatus_.SrcImage)
            {
                case "/images/confirmed.png":
                    caseService.CaseConfirmed(id_);
                    await Clients.Group(group).SendAsync("changeButton", imgStatus_);
                    break;
                case "/images/no_complete.png":
                    caseService.FailedCaseTeam(id_);
                    await Clients.Group(group).SendAsync("changeButton", imgStatus_);
                    break;
                case "cancel":
                    caseService.CancelCaseTeam(id_);
                    break;
                case "/images/complete.jpg":
                    caseService.CompleteCase(id_);
                    await Clients.OthersInGroup(group).SendAsync("changeButton", imgStatus_);
                    break;
            }
                
            await Clients.Group(group).SendAsync("changeStatus", imgStatus_);
        }
    }

    public class ImgStatus
    {
        public string IdCase { get; set; }
        public string ClassName { get; set; }
        public string SrcImage { get; set; }
        public string AltImage { get; set; }
    }
}
