using Controllers.ViewModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using ToDo2.DataBase;
using ToDo2.Models.Service;

namespace UnitTest
{
    [TestClass]
    public class UnitTestUser
    {
        UserService userService;
        ToDoContext toDoContext;
        CompanyService companyService;
        public UnitTestUser()
        {
            toDoContext = new ToDoContext();
            userService = new UserService(toDoContext);
            companyService = new CompanyService(toDoContext);
        }

        /// <summary>
        /// �������� ������� ������������
        /// </summary>
        [TestMethod]
        public void TestMethodUserCredentials()
        {
            RegisterViewModel user = new RegisterViewModel();
            user.Email = "nikita@gmail.com";
            user.Company = "Nikita Co.";
            user.Password = "123456";
            user.ConfirmPassword = "123456";
            Company company = companyService.AddCompany("TestCompany");
            userService.AddUser(user.Email, user.Password, company);
            ClientToDo client = userService.GetUser("nikita@gmail.com");            
            Assert.AreEqual("nikita@gmail.com", client.Name);
            Assert.AreEqual(1, client.IdCompany);
            Assert.AreEqual(null, client.IdTeam);

            Credentials credentials = userService.GetCredentials("nikita@gmail.com");
            Assert.AreEqual("nikita@gmail.com", credentials.Login);
            Assert.AreEqual("123456", credentials.Password);
        }

        /// <summary>
        /// ���������� ������
        /// </summary>
        [TestMethod]
        public void TestMethodPersonalData()
        {
            PersonalAreaViewModel user = new PersonalAreaViewModel();
            user.Email = "admin@mail.ru";
            user.Name = "admin";
            user.Login = "admin@mail.com";
            user.Password = "1234567";
            userService.UpdatePersonalData(user.Name, user.Email, user.Login, user.Password, "admin@mail.ru");

            ClientToDo client = userService.GetUser("admin@mail.com");
            Credentials credentials = userService.GetCredentials("admin@mail.com");
            Assert.AreEqual("admin", client.Name);
            Assert.AreEqual("admin@mail.com", credentials.Login);
            Assert.AreEqual("1234567", credentials.Password);
            /////////////////////////////////////////////////////
            user.Email = "admin@mail.com";
            user.Name = "admin@mail.com";
            user.Login = "admin@mail.ru";
            user.Password = "123456";
            userService.UpdatePersonalData(user.Name, user.Email, user.Login, user.Password, "admin@mail.com");

            client = userService.GetUser("admin@mail.ru");
            credentials = userService.GetCredentials("admin@mail.ru");
            Assert.AreEqual("admin@mail.com", client.Name);
            Assert.AreEqual("admin@mail.ru", credentials.Login);
            Assert.AreNotEqual("1234567", credentials.Password);
            Assert.AreEqual("123456", credentials.Password);
        }
    }
}
