﻿using Controllers.ViewModel;
using Controllers.ViewModel.Team_;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using ToDo2.DataBase;
using ToDo2.Models.Service;

namespace UnitTest
{
    /// <summary>
    /// Тестирование групп
    /// </summary>
    [TestClass]
    public class UnitTestTeam
    {
        UserService userService;
        ToDoContext toDoContext;
        TeamService teamService;
        CompanyService companyService;
        public UnitTestTeam()
        {
            toDoContext = new ToDoContext();
            userService = new UserService(toDoContext);
            teamService = new TeamService(toDoContext);
            companyService = new CompanyService(toDoContext);

            СreateTestUserInCompanyInTeam();
            СreateTestUserWithoutCompanyInTeam();
            СreateTestUserInCompanyWithoutTeam();
            СreateTestUserWithoutCompanyWithoutTeam();
        }

        private void СreateTestUserWithoutCompanyWithoutTeam()
        {
            // Тестовый пользователь в команде "TestTeam1"
            RegisterViewModel testUser1 = new RegisterViewModel();
            testUser1.Email = "testClient7@gmail.com";
            testUser1.Company = null;
            testUser1.Password = "123456";
            testUser1.ConfirmPassword = "123456";
            Company company = null;
            userService.AddUser(testUser1.Email, testUser1.Password, company);

            // Тестовый пользователь в команде "TestTeam1"
            RegisterViewModel testUser2 = new RegisterViewModel();
            testUser2.Email = "testClient8@gmail.com";
            testUser2.Company = null;
            testUser2.Password = "123456";
            testUser2.ConfirmPassword = "123456";
            userService.AddUser(testUser2.Email, testUser2.Password, company);
        }

        private void СreateTestUserInCompanyWithoutTeam()
        {
            // Тестовый пользователь в команде "TestTeam1"
            RegisterViewModel testUser1 = new RegisterViewModel();
            testUser1.Email = "testClient5@gmail.com";
            testUser1.Company = "TestCompany2";
            testUser1.Password = "123456";
            testUser1.ConfirmPassword = "123456";
            Company company = companyService.AddCompany("TestCompany2");
            userService.AddUser(testUser1.Email, testUser1.Password, company);
            // Тестовый пользователь в команде "TestTeam1"
            RegisterViewModel testUser2 = new RegisterViewModel();
            testUser2.Email = "testClient6@gmail.com";
            testUser2.Company = "TestCompany2";
            testUser2.Password = "123456";
            testUser2.ConfirmPassword = "123456";
            userService.AddUser(testUser2.Email, testUser2.Password, company);
        }

        private void СreateTestUserInCompanyInTeam()
        {
            //Создаем тестовую команду
            TeamViewModel teamModel = new TeamViewModel();
            teamModel.Name = "TestTeam1";
            // Тестовый пользователь в команде "TestTeam1"
            RegisterViewModel testUser1 = new RegisterViewModel();
            testUser1.Email = "testClient1@gmail.com";
            testUser1.Company = "TestCompany";
            testUser1.Password = "123456";
            testUser1.ConfirmPassword = "123456";
            Company company = companyService.AddCompany("TestCompany");
            userService.AddUser(testUser1.Email, testUser1.Password, company);
            Credentials testClient1Credentials = userService.GetCredentials(testUser1.Email);
            //Добавляем в команду
            teamService.AddTeam(teamModel.Name, testClient1Credentials.Login);
            // Тестовый пользователь в команде "TestTeam1"
            RegisterViewModel testUser2 = new RegisterViewModel();
            testUser2.Email = "testClient2@gmail.com";
            testUser2.Company = "TestCompany";
            testUser2.Password = "123456";
            testUser2.ConfirmPassword = "123456";
            userService.AddUser(testUser2.Email, testUser2.Password, company);
            Credentials testClient2Credentials = userService.GetCredentials(testUser2.Email);
            Team team = teamService.GetTeam(testClient1Credentials.Login);
            teamService.JoinTeam(team.Id, testClient2Credentials.Login);
        }

        private void СreateTestUserWithoutCompanyInTeam()
        {
            // Создаем тестовую команду
            TeamViewModel teamModel = new TeamViewModel();
            teamModel.Name = "TestTeam3";
            // Тестовый пользователь в команде "TestTeam2"
            RegisterViewModel testUser3 = new RegisterViewModel();
            testUser3.Email = "testClient3@gmail.com";
            testUser3.Company = null;
            testUser3.Password = "123456";
            testUser3.ConfirmPassword = "123456";
            Company company = null;
            userService.AddUser(testUser3.Email, testUser3.Password, company);
            Credentials testClient1Credentials = userService.GetCredentials(testUser3.Email);
            //Добавляем команду
            teamService.AddTeam(teamModel.Name, testClient1Credentials.Login);
            // Тестовый пользователь в команде "TestTeam2"
            RegisterViewModel testUser4 = new RegisterViewModel();
            testUser4.Email = "testClient4@gmail.com";
            testUser4.Company = null;
            testUser4.Password = "123456";
            testUser4.ConfirmPassword = "123456";

            userService.AddUser(testUser4.Email, testUser4.Password, company);
            Credentials testClient2Credentials = userService.GetCredentials(testUser4.Email);
            Team team = teamService.GetTeam(testClient1Credentials.Login);
            teamService.JoinTeam(team.Id, testClient2Credentials.Login);
        }

        /// <summary>
        /// Проверка функции на возвращение пользователей без группы и компании
        /// </summary>
        [TestMethod]
        public void TestMethodGetUsersWithoutCompanyWithoutTeam()
        {
            var users = teamService.GetUsersWithoutCompanyWithoutTeam();
            Assert.AreEqual("testClient7@gmail.com", users[1].Name);
            Assert.AreEqual("testClient8@gmail.com", users[2].Name);
            Assert.AreEqual(null, users[1].IdCompany);
            Assert.AreEqual(null, users[2].IdCompany);
            Assert.AreEqual(null, users[1].IdTeam);
            Assert.AreEqual(null, users[2].IdTeam);
        }

        /// <summary>
        /// Проверка функции на возвращение пользователей без группы, но в компании
        /// </summary>
        [TestMethod]
        public void TestMethodGetUsersInCompanyWithoutTeam()
        {
            var users = teamService.GetUsersInCompanyWithoutTeam(2);
            Assert.AreEqual("testClient5@gmail.com", users[0].Name);
            Assert.AreEqual("testClient6@gmail.com", users[1].Name);
            Assert.AreEqual(2, users[0].IdCompany);
            Assert.AreEqual(2, users[1].IdCompany);
            Assert.AreEqual(null, users[0].IdTeam);
            Assert.AreEqual(null, users[1].IdTeam);
        }

        /// <summary>
        /// Проверка функции на возвращение пользователей без компании, но в группе
        /// </summary>
        [TestMethod]
        public void TestMethodGetUsersWithoutCompanyInTeam()
        {
            var users = teamService.GetUsersWithoutCompanyInTeam(2);
            Assert.AreEqual("testClient3@gmail.com", users[0].Name);
            Assert.AreEqual("testClient4@gmail.com", users[1].Name);
            Assert.AreEqual(null, users[0].IdCompany);
            Assert.AreEqual(null, users[1].IdCompany);
            Assert.AreEqual(2, users[0].IdTeam);
            Assert.AreEqual(2, users[1].IdTeam);
        }

        /// <summary>
        /// Проверка функции на возвращение пользователей в компании, но в группе
        /// </summary>
        [TestMethod]
        public void TestMethodGetUsersInCompanyInTeam()
        {
            var users = teamService.GetUsersInCompanyInTeam(1 ,1);
            Assert.AreEqual("testClient1@gmail.com", users[0].Name);
            Assert.AreEqual("testClient2@gmail.com", users[1].Name);
            Assert.AreEqual(1, users[0].IdCompany);
            Assert.AreEqual(1, users[1].IdCompany);
            Assert.AreEqual(1, users[0].IdTeam);
            Assert.AreEqual(1, users[1].IdTeam);
        }

        /// <summary>
        /// Тест на удаление члена команды
        /// </summary>
        [TestMethod]
        public void TestMethodDeleteMember()
        {
            var users = teamService.GetUsersInCompanyInTeam(1, 1);
            ClientToDo tmp = users[1];
            Assert.AreEqual(2, users.Count);
            teamService.DeleteTeamMember(users[1].Id);
            users = teamService.GetUsersInCompanyInTeam(1, 1);
            Assert.AreEqual(1, users.Count);
            TestMethodJoinMember(tmp.Name, users);
        }

        /// <summary>
        /// Тест на добавление члена команды
        /// </summary>
        [TestMethod]
        private void TestMethodJoinMember(string login, List<ClientToDo> users)
        {
            teamService.JoinTeam(1, login);
            users = teamService.GetUsersInCompanyInTeam(1, 1);
            Assert.AreEqual(2, users.Count);
        }
    }
}
