﻿using Controllers.ViewModel.Board;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToDo2.DataBase;
using ToDo2.Models;
using ToDo2.Models.Service;

namespace UnitTest
{
    [TestClass]
    public class UnitTestCase
    {
        UserService userService;
        ToDoContext toDoContext;
        CaseService caseService;
        public UnitTestCase()
        {
            toDoContext = new ToDoContext();
            userService = new UserService(toDoContext);
            caseService = new CaseService(toDoContext);
        }

        /// <summary>
        /// Запускать только после теста TestMethodUserCredentials
        /// </summary>
        [TestMethod]
        public void TestMethodAddCaseWithoutTeam()
        {
            BoardViewModel model = new BoardViewModel()
            {
                Title = "TestTitle",
                Start = "10.10.2010 00:00:00",
                End = "11.10.2010 00:00:00",
                Description = "Description test",
                IsComplete = StatusCase.В_процессе.ToString(),
                Private = true,
                Team = null
            };
            bool isTrue = caseService.AddCase(model.Start, model.End, model.Title, model.Description, "admin@mail.ru");
            var cases = caseService.GetCases("admin@mail.ru");
            Assert.AreEqual("TestTitle", cases.ElementAt(0).Title);
            Assert.AreEqual(true, isTrue);
            Assert.AreEqual("10.10.2010 0:00:00", cases.ElementAt(0).Start.ToString());
            Assert.AreEqual("11.10.2010 0:00:00", cases.ElementAt(0).End.ToString());
            Assert.AreEqual("Description test", cases.ElementAt(0).Description);
            Assert.AreEqual("В_процессе", cases.ElementAt(0).IsComplete);
            Assert.AreEqual(1, cases.ElementAt(0).IdUser);

            TestMethodAddCaseOperationConfirmedWithoutTeam(cases.ElementAt(0));
            TestMethodAddCaseOperationAcceptedWithoutTeam(cases.ElementAt(0));
            TestMethodAddCaseOperationCompleteWithoutTeam(cases.ElementAt(0));
            TestMethodAddCaseOperationFailedWithoutTeam(cases.ElementAt(0));
            caseService.CancelCase(cases.ElementAt(0).Id);
        }

        /// <summary>
        /// Проверка операции сonfirmed
        /// </summary>
        /// <param name="case_">задача</param>
        [TestMethod]
        private void TestMethodAddCaseOperationConfirmedWithoutTeam(Case case_)
        {
            caseService.CaseConfirmed(case_.Id);
            Assert.AreEqual("Подтверждено", case_.IsComplete);
        }

        /// <summary>
        /// Проверка операции Accepted
        /// </summary>
        /// <param name="case_">задача</param>
        [TestMethod]
        private void TestMethodAddCaseOperationAcceptedWithoutTeam(Case case_)
        {
            caseService.AcceptedCase(case_.Id);
            Assert.AreEqual("Принято", case_.IsComplete);
        }

        /// <summary>
        /// Проверка операции Complete
        /// </summary>
        /// <param name="case_">задача</param>
        [TestMethod]
        private void TestMethodAddCaseOperationCompleteWithoutTeam(Case case_)
        {
            caseService.CompleteCase(case_.Id);
            Assert.AreEqual("Сделано", case_.IsComplete);
        }

        /// <summary>
        /// Проверка операции failed
        /// </summary>
        /// <param name="case_">задача</param>
        [TestMethod]
        private void TestMethodAddCaseOperationFailedWithoutTeam(Case case_)
        {
            caseService.FailedCaseTeam(case_.Id);
            Assert.AreEqual("Провалено", case_.IsComplete);
        }
    }
}
